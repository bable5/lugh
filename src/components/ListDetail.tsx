import { Layout, theme } from "antd";

const { Content, Sider } = Layout;
import { ElementType } from "react";

type ListDetailProps = {
    list: ElementType,
    detail: ElementType,
}

export default function (props: ListDetailProps) {
    const {
        token: { colorBgContainer },
    } = theme.useToken();

    return (
        <Content style={{maxHeight: "100vh"}}>
            <Layout style={{ padding: '24px 0', background: colorBgContainer }}>
                <Sider style={{ background: colorBgContainer }}>
                    <props.list />
                </Sider>
                <Content>
                    <props.detail />
                </Content>
            </Layout>
        </Content>
    );

}
