import { ReactNode } from "react";
import './index.css';

type ScrollViewProps = {
    children: ReactNode
}

export default ({ children }: ScrollViewProps) => {
    return (
        <div className="scroll-view">
            {children}
        </div>
    );
};