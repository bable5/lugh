import TextArea from "antd/es/input/TextArea";

export type Props = {
    obj: Record<any, any>
}

export default ({obj}: Props) => (<TextArea
    readOnly
    autoSize
    value={JSON.stringify(obj, null, 2)}
    style={{
        fontFamily: 'monospace'
    }}
/>);