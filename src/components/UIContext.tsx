import { ConfigProvider, theme } from "antd";
import { ReactNode } from "react";

type UIConfigProviderProps = {
    children: ReactNode
}

export default ({ children }: UIConfigProviderProps): ReactNode => (
    <ConfigProvider
        theme={{
            algorithm: [theme.compactAlgorithm],
        }}
    >
        {children}
    </ConfigProvider>
);