import { Flex } from "antd";
import EnvironmentSelector from "../../environment/EnvironmentSelector";

export default function (): React.JSX.Element {
    return (
        <Flex>
            <EnvironmentSelector />
        </Flex>
    );
}
