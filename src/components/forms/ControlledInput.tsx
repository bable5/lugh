import { observer } from "mobx-react-lite";
import {Input} from 'antd';
import {useCallback} from 'react';

const ControlledInput = observer(
    ({ name, value, onChange }: { name: string, value: string, onChange: (s: string) => void }) => {
        const onChangeCallback = useCallback((e: React.ChangeEvent<HTMLInputElement>) => onChange(e.target.value), [onChange]);

        return <Input
            name={name}
            value={value}
            onChange={onChangeCallback}
        />;
    });

export default ControlledInput;