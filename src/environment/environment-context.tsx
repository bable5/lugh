import { createContext } from "react";
import { EnvironmentsStore } from "../store/environments-store";

export class EnvironmentsScreenContext {
    private _environmentsStore: EnvironmentsStore;

    constructor(environmentsStore: EnvironmentsStore) {
        this._environmentsStore = environmentsStore;
    }

    get environmentsStore(): EnvironmentsStore {
        return this._environmentsStore;
    }
}

export const EnvironmentsContext: React.Context<EnvironmentsScreenContext>
    = createContext(undefined);
