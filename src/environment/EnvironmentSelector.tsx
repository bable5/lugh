import { observer } from "mobx-react-lite";
import { useContext } from "react";
import { EnvironmentsContext } from "./environment-context";
import { Select } from "antd";

export default observer(() => {
    const { environmentsStore } = useContext(EnvironmentsContext);
    //TODO: This could move into the environments view store to
    // make this mapping reactive
    const options = environmentsStore.environmentNames.map(e => ({
        value: e,
        label: e
    }));
    return (<Select
        style={{ width: 120 }}
        value={environmentsStore.selectedEnvironmentName}
        options={options}
        onChange={(e: string) => environmentsStore.selectedEnvironmentName = e}
    />);
});
