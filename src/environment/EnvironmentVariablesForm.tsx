import { Button, Col, Row } from "antd";
import { observer } from "mobx-react-lite";
import { ReactNode, useContext } from "react";
import ControlledInput from "../components/forms/ControlledInput";
import { Environment } from "../store/environments-store";
import { EnvironmentsContext } from "./environment-context";

const toForm = (environment: Environment,
    onUpdateKey: (index: number, newValue: string) => void,
    onUpdateValue: (index: number, newValue: string) => void,
    onDeleteVariable: (index: number) => void
    ): ReactNode[] => {
    return environment.variables.map(({name, value}, i) => {
        return <Row key={i}>
            <Col>
                <ControlledInput
                    name={`${name}`}
                    value={name}
                    onChange={(v) => onUpdateKey(i, v)} />
            </Col>
            <Col>
                <ControlledInput
                    name={`${name}${value}`}
                    value={value}
                    onChange={(v) => onUpdateValue(i, v)}
                />
            </Col>
            <Col>
                <Button title="Delete"
                onClick={() => onDeleteVariable(i)}
                >-</Button>
            </Col>
        </Row>;
    });
};

export default observer(() => {
    const { environmentsStore } = useContext(EnvironmentsContext);
    const selectedEnvironment = environmentsStore.selectedEnvironment;
    if (!selectedEnvironment) {
        return undefined;
    }

    const onUpdateKey = environmentsStore.updateEnvironmentVariableKey;
    const onUpdateValue = environmentsStore.updateEnvironmentVariableValue;
    const onDeleteVariable = environmentsStore.deleteEnvironmentVariable;

    return (
        <>
            {toForm(selectedEnvironment, onUpdateKey, onUpdateValue, onDeleteVariable)}
        </>
    );

});
