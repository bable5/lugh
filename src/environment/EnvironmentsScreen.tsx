import { Button, Layout } from "antd";
import { observer } from "mobx-react-lite";
import { useContext } from "react";
import ScrollView from "../components/ScrollView";
import EnvironmentVariablesForm from "./EnvironmentVariablesForm";
import { EnvironmentsContext } from "./environment-context";

export default observer(() => {

    const { environmentsStore } = useContext(EnvironmentsContext);
    const addDisabled = !environmentsStore.selectedEnvironmentName;
    return (
        <Layout>
            <Layout.Content>
                <Button
                    onClick={environmentsStore.newEnvironmentVariable}
                    disabled={addDisabled}>Add</Button>
                <ScrollView>
                    <EnvironmentVariablesForm />
                </ScrollView>
            </Layout.Content>
        </Layout>
    );
});
