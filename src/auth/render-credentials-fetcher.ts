// TODO: This belongs somewhere else. It's part of the renderer process.
export async function invokeFetchAuth(auth: any): Promise<string> {
    const res = (window as any)
        .electronAPI
        .onFetchAuth(auth);
    return await res;
}