import Oauth2Form from "./Oauth2Form";

const CurrentForm = () => {
    return <Oauth2Form />;
};

export default function () {
    return <div className="bordered">
        <CurrentForm />
    </div>;
}