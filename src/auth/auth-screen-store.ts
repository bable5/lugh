import { computed, makeAutoObservable } from "mobx";
import { Auth, AuthStore } from "../store/auth-store";

export class AuthScreenStore {
    _visibleAuthFilter: string;
    _authStore: AuthStore;

    constructor(authStore: AuthStore) {
        makeAutoObservable(this, {
            visibleAuth: computed
        }, { autoBind: true });
        this._authStore = authStore;
        this._visibleAuthFilter = "";
    }

    get visibleAuthFilter(): string {
        return this._visibleAuthFilter;
    }

    set visibleAuthFilter(visibleAuthFilter: string) {
        this._visibleAuthFilter = visibleAuthFilter;
    }

    get visibleAuth(): Auth[] {
        return this._authStore.savedAuth
            .filter(a =>
                this._visibleAuthFilter
                    ? a.name.includes(this._visibleAuthFilter)
                    : a);
    }
}