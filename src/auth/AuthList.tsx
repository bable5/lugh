import { Layout, Menu } from "antd";
import { observer } from "mobx-react-lite";
import { useCallback, useContext } from "react";
import ControlledInput from "../components/forms/ControlledInput";
import ScrollView from "../components/ScrollView";
import { AuthType } from "../store/auth-store";
import { AuthContext } from "./auth-context";

const AuthListFilter = observer(() => {
    const { authScreenStore } = useContext(AuthContext);
    const doFilter = (v: string) => authScreenStore.visibleAuthFilter = v;

    return <ControlledInput
        name="auth-filter"
        value={authScreenStore.visibleAuthFilter}
        onChange={doFilter}
    />;
});

const AuthListHeader = () => {
    const { authStore } = useContext(AuthContext);
    const onClickHandler = useCallback(() => authStore.createNewAuth(AuthType.Oauth2), []);

    return (
        <button onClick={onClickHandler}>+</button>
    );
};

const AuthMenu = observer(() => {
    const { authStore, authScreenStore } = useContext(AuthContext);
    const visibleAuth = authScreenStore.visibleAuth.map(auth => ({
        key: auth.id,
        label: auth.name
    }));

    return (
        <Layout.Content>
            <AuthListFilter />
            <ScrollView>
                <Menu id="auth-items"
                    items={visibleAuth}
                    onSelect={({ key }) => authStore.selectAuthById(key)}
                />
            </ScrollView>
        </Layout.Content>
    );
});

export default observer(() => {
    return <Layout>
        <AuthListHeader />
        <AuthMenu />
    </Layout>;
}
);