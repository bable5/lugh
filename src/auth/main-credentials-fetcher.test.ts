import Chance from 'chance';
import { IpcMainInvokeEvent, ipcMain } from 'electron';
import { AuthType, ClientCredentialsAuth, Oauth2Types } from '../store/auth-store';
import { vi } from 'vitest';
import { registerIpcMainHandler } from './main-credentials-fetcher';
import 'vitest-fetch-mock';


const chance = new Chance();
let handler: (event: IpcMainInvokeEvent, ...args: any[])=>void;

vi.mock(
    'electron',
    () => {
      const mockIpcMain = {
        on: vi.fn().mockReturnThis(),
        handle: (channel: string, listener: (event: IpcMainInvokeEvent, ...args: any[])=>void) => {
            handler = listener;
        },
      };
      return { ipcMain: mockIpcMain };
    },
  );

test("Request Client Credentials", async () => {
    registerIpcMainHandler(ipcMain, 'oauth-2-credentials');

    const clientlientCredentials: ClientCredentialsAuth = {
      authType: AuthType.Oauth2,
      credentialType: Oauth2Types.ClientCredentials,
      id: chance.guid(),
      clientKey: chance.string(),
      clientSecret: chance.string(),
      tokenUrl: chance.url()
    };

    fetchMock.mockResponse(JSON.stringify({
      "access_token": chance.string()
    }));

    await handler({} as IpcMainInvokeEvent, clientlientCredentials);

    expect(fetchMock.requests().length)
      .toEqual(1);
    expect(fetchMock.requests()[0].url)
      .toEqual(`${clientlientCredentials.tokenUrl}`);
    const actualBody = await new Response(fetchMock.requests()[0].body).text();
    expect(actualBody)
      .toEqual('grant_type=client_credentials');
});