import { createContext } from 'react';
import { AuthStore } from '../store/auth-store';
import { AuthScreenStore } from './auth-screen-store';

export class AuthScreenContext {
    private _authStore: AuthStore;
    private _authScreenStore: AuthScreenStore;

    constructor(authStore: AuthStore) {
        this._authStore = authStore;
        this._authScreenStore = new AuthScreenStore(authStore);
    }

    get authStore(): AuthStore {
        return this._authStore;
    }

    get authScreenStore(): AuthScreenStore {
        return this._authScreenStore;
    }
}

export const AuthContext: React.Context<AuthScreenContext> = createContext(undefined);
