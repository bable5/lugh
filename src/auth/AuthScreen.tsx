import AuthList from "./AuthList";
import AuthDetail from "./AuthDetail";
import ListDetail from "../components/ListDetail";

export default function () {
    return (
        <ListDetail list={AuthList} detail={AuthDetail} />
    );
}
