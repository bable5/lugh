import { expect, test } from 'vitest';
import { Auth, AuthStore, AuthType } from '../store/auth-store';
import { AuthScreenContext } from './auth-context';

test("Adding a new request is visible should be visible", () => {
    const store = givenARequestsContext();

    store.authStore.createNewAuth(AuthType.Basic);

    expect(store.authScreenStore.visibleAuth)
        .to.toHaveLength(1);
});

test("Adding a filter should hide auth that does not match", () => {
    const store = givenARequestsContext();

    store.authStore.createNewAuth(AuthType.Basic);

    const auth = Object.values(store.authStore.savedAuth)[0];
    auth.name = "FOO";
    store.authScreenStore.visibleAuthFilter = "B";

    expect(store.authScreenStore.visibleAuth)
        .to.toHaveLength(0);
});

test("Should include everything when the filter is empty", () => {
    const store = givenARequestsContext();

    store.authStore.createNewAuth(AuthType.Basic);

    const auth = Object.values(store.authStore.savedAuth)[0];
    auth.name = "FOO";
    store.authScreenStore.visibleAuthFilter = "";

    expect(store.authScreenStore.visibleAuth)
        .to.toHaveLength(1);
});

test("Should include everything if the filter is undefined", () => {
    const store = givenARequestsContext();

    store.authStore.createNewAuth(AuthType.Basic);

    const auth = Object.values(store.authStore.savedAuth)[0];
    auth.name = "FOO";
    store.authScreenStore.visibleAuthFilter = undefined;

    expect(store.authScreenStore.visibleAuth)
        .to.toHaveLength(1);
});

const givenARequestsContext = (): AuthScreenContext => {

    const mockAuthStore = new AuthStore({
        load: () => Promise.resolve([] as Auth[]),
        save: vi.fn()
    });
    return new AuthScreenContext(mockAuthStore);
};