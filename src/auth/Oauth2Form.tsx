import { Button, Flex, Input, Select, Space, Typography } from 'antd';
import { observer } from "mobx-react-lite";
import { useCallback, useContext } from "react";
import ControlledInput from "../components/forms/ControlledInput";
import { AuthType, Oauth2Auth, Oauth2Types } from "../store/auth-store";
import { AuthContext } from "./auth-context";

const { Text, Title } = Typography;

const oauth2Types = [
    {
        value: Oauth2Types.ClientCredentials,
        label: 'Client Credentials'
    }, {
        value: Oauth2Types.AuthorizationToken,
        label: 'Authorization Token'
    }
];

const Oauth2CredentialTypeSelect = observer(() => {
    const { authStore: { selectedAuth } } = useContext(AuthContext);
    const oauth2Auth = selectedAuth as Oauth2Auth; //TODO: Let's not cast all over the place?

    const onSelect = useCallback((value: Oauth2Types) =>
        oauth2Auth.credentialType = value,
        [selectedAuth]);

    return (<>
        <Text>Credential Type</Text>
        <Select
            value={oauth2Auth.credentialType}
            defaultValue={Oauth2Types.AuthorizationToken}
            options={oauth2Types}
            onChange={onSelect}
        />
    </>);
});

const Oauth2Form = observer(() => {
    const { authStore } = useContext(AuthContext);

    if (authStore.selectedAuth.authType === AuthType.Oauth2) {
        return <AuthorizationTokenForm />;
    }
    // TODO: Error handling?
    return <Text>Tried to generate oauth2 form from non-oauth2 selected auth</Text>;
});

const AuthorizationTokenForm = observer(() => {
    const { authStore } = useContext(AuthContext);

    const selectedAuth = authStore.selectedAuth as Oauth2Auth;
    const isAuthorizationToken = selectedAuth.credentialType === Oauth2Types.AuthorizationToken;

    return <>
        <Oauth2CredentialTypeSelect />
        {isAuthorizationToken ?
            <div className="form-row"><Text>Authorization URL:</Text>
                <ControlledInput name="authorization-url" value={selectedAuth.authorizationUrl} onChange={selectedAuth.updateAuthorizationUrl} />
            </div>
            : undefined
        }
        <div className="form-row">
            <Text>Token URL:</Text>
            <ControlledInput name="token-url" value={selectedAuth.tokenUrl} onChange={selectedAuth.updateTokenUrl} />
        </div>
        <div className="form-row">
            <Text>Client Key:</Text>
            <ControlledInput name="client-key" value={selectedAuth.clientKey} onChange={selectedAuth.updateCientKey} />
        </div>
        <div className="form-row">
            <Text>Client Secret:</Text>
            <ControlledInput name="client-secret" value={selectedAuth.clientSecret} onChange={selectedAuth.updateClientSecret} />
        </div>
        {isAuthorizationToken ?
            <div className="form-row">
                <Text>Callback:</Text>
                <ControlledInput name="callback" value={selectedAuth.callbackUrl} onChange={selectedAuth.updateCallbackUrl} />
            </div>
            : undefined
        }
        <div className="form-row">
            <Text>Scopes:</Text>
            <ControlledInput name="scopes" value={selectedAuth.scopes} onChange={selectedAuth.updateScopes} />
        </div>
        <div className="form-row">
            <Text>Nonce:</Text>
            <ControlledInput name="nonce" value={selectedAuth.nonce} onChange={selectedAuth.updateNonce} />
        </div>
        <Space >
            <div className="form-row"><Button type="primary" htmlType="submit">Fetch Token</Button></div>
        </Space>
        <div>
            <div className="form-row"><Text>Token:</Text><Input readOnly={true} value={selectedAuth.token} />
                <Button onClick={() => {
                    navigator.clipboard.writeText(selectedAuth.token);
                }}>Copy</Button>
            </div>

        </div>
    </>;
});

export default observer(() => {
    const { authStore } = useContext(AuthContext);

    const selectedAuth = authStore.selectedAuth;
    if (!selectedAuth) {
        return <Flex justify='center'><Title level={2}>Edit Existing or Create New</Title></Flex>;
    }

    const handleSubmit = (event: any) => { //TODO: Type of form submit event
        event.preventDefault();
        authStore.fetchAuth();
    };

    return <>
        <form onSubmit={handleSubmit}>
            <Flex vertical>
                <div className="form-row"><label>Name:</label><ControlledInput name="name" value={selectedAuth.name}
                    onChange={selectedAuth.updateName}
                /></div>
                {selectedAuth.authType === AuthType.Oauth2
                    ? <Oauth2Form />
                    : <span>UNSUPPORTED AUTH TYPE {selectedAuth.authType}</span>
                }
            </Flex>
        </form>
    </>;
});