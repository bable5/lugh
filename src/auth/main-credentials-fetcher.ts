/**
 * IPC handler to fetch Oauth2 client creds or an authorization token.
 *
 * An auth token requires opening a new window and navigating the auth
 * provider's signin page, then intercepting the redirect to the callback
 * url.
 */

import { BrowserWindow } from "electron";
import { ClientCredentialsAuth, UserCredentialsAuth } from "../store/auth-store";

export function registerIpcMainHandler(ipcMain: Electron.IpcMain, eventId: string) {
    ipcMain.handle(eventId, handleFetchOauth2Token);
}

async function handleFetchOauth2Token(event: any, arg: UserCredentialsAuth) {
    if (arg.credentialType === "CLIENT_CREDENTIALS") {
        return fetchClientCredentials(arg);
    } else if (arg.credentialType === "AUTHORIZATION_TOKEN") {
        return fetchAuthorizationToken(arg);
    } else {
        console.log(`Unknown credential type ${arg.credentialType}`);
        return "UNSUPPORTED CREDENTIAL TYPE!";
    }
}

async function fetchClientCredentials(credentials: ClientCredentialsAuth): Promise<string> {

    const basicAuth = toBasicAuth(credentials.clientKey, credentials.clientSecret);

    const url = new URL(credentials.tokenUrl);
    const authParms = new URLSearchParams();
    authParms.append("grant_type", "client_credentials");

    //Todo: Scope, if applicable
    const tokenResp = await fetch(url, {
        method: "POST",
        headers: {
            "cache-control": "no-cache",
            "content-type": "application/x-www-form-urlencoded",
            "accept": "application/json",
            "authorization": basicAuth
        },
        body: authParms.toString(),
    });

    if (tokenResp.ok) {
        const body = await tokenResp.json();
        const accessToken = body["access_token"];
        return accessToken;
    } else {
        const text = await tokenResp.text();
        console.log(tokenResp.status, tokenResp.statusText);
        console.log(text);
        throw new Error(`Auth failed ${tokenResp.status}, ${tokenResp.statusText} ${text}`);
    }
}

async function fetchAuthorizationToken(credentials: UserCredentialsAuth): Promise<string> {
    let resCapture: (value: any) => void, rejectCapture: (value: any) => void;
    const eventualResult: Promise<string> = new Promise((res, reject) => {
        resCapture = res;
        rejectCapture = reject;
    });

    const authWindow = new BrowserWindow({
        width: 800,
        height: 600,
        show: false,
        modal: true
    });

    const authorizeUrl = new URL(credentials.authorizationUrl);
    authorizeUrl.searchParams.append("response_type", "code");
    authorizeUrl.searchParams.append("client_id", credentials.clientKey);
    authorizeUrl.searchParams.append("state", credentials.nonce);
    authorizeUrl.searchParams.append("redirect_uri", credentials.callbackUrl);
    if (credentials.scopes) {
        authorizeUrl.searchParams.append("scope", credentials.scopes);
    }

    authWindow.loadURL(authorizeUrl.toString());
    authWindow.webContents.on('will-redirect', async (event, newUrl) => {
        console.log(event);
        console.log(newUrl);
        if (newUrl.startsWith(credentials.callbackUrl)) {
            event.preventDefault();

            //TODO: It's aysnc. try/catch something?
            //TODO: In general, error handling. Response code, etc.

            const urlParts = newUrl.split("?");
            if (urlParts.length < 2) {
                rejectCapture(`No query parameters returned '${newUrl}'`);
                authWindow.close();
                return;
            }

            //TODO: Check the response nonce/state
            const params = new URLSearchParams(urlParts[1]);


            const code = params.get("code");
            if (!code) {
                rejectCapture("Retrieving code failed!");
                // TODO: Need a 'goto exit' type thing for cleanup....
                //   Probably in the resolve/reject functions up above?
                authWindow.close();
                return;
            }

            const authTokenUrl = new URL(credentials.tokenUrl);
            const authParams = new URLSearchParams();
            authParams.append("grant_type", "authorization_code");
            authParams.append("code", code);
            authParams.append("redirect_uri", credentials.callbackUrl);

            const basicAuth = toBasicAuth(credentials.clientKey, credentials.clientSecret);
            const tokenResp = await fetch(authTokenUrl, {
                method: "POST",
                headers: {
                    "content-type": "application/x-www-form-urlencoded",
                    "accept": "application/json",
                    "authorization": basicAuth
                },
                body: authParams.toString()
            });

            if (tokenResp.ok) {
                const body = await tokenResp.json();
                const accessToken = body["access_token"];
                resCapture(accessToken);
            } else {
                const text = await tokenResp.text();
                console.log(tokenResp.status, tokenResp.statusText);
                console.log(text);
                rejectCapture(`Auth failed ${tokenResp.status}, ${tokenResp.statusText} ${text}`);
            }

            authWindow.close();
        }
    });

    authWindow.show();

    return eventualResult;
}

const toBasicAuth = (username: string, password: string): string => {
    const creds = Buffer.from(`${username}:${password}`).toString('base64');
    return `Basic: ${creds}`;
};