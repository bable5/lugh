const sum = (a: number, b: number) => a + b;

// A smoke test to prove things are somewhat configured
test('adds 1 + 2 is 3', () => {
    expect(sum(1, 2))
        .toBe(3);
});
