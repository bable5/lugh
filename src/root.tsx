import ReactDOM from "react-dom/client";
import React from "react";
import App from "./app";
import '@ant-design/v5-patch-for-react-19';
import { navigation } from './navigation';
import { AuthStore } from "./store/auth-store";
import { AuthContext, AuthScreenContext } from "./auth/auth-context";
import { RequestsContext, RequestsScreenContext } from "./requests/requests-context";
import { loadAuth, saveAuth, loadRequests, saveRequest } from "./store/renderer-persistence";
import { RequestsStore } from "./store/requests-store";
import { fetchHttpRequest } from "./requests/render-fetch";
import UIContext from "./components/UIContext";
import { EnvironmentsContext, EnvironmentsScreenContext } from "./environment/environment-context";
import { Environment, EnvironmentsStore } from "./store/environments-store";

const authStore = new AuthStore({
    load: loadAuth,
    save: saveAuth
});

const environmentsStore = new EnvironmentsStore({
    load: () => Promise.resolve({
        default: {
            name: "default",
            variables: [
                {
                    name: "host",
                    value: "localhost:8081"
                }
            ]
        }
    }),

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    save: (e: Environment) => Promise.resolve(null),
});

const authScreenContext = new AuthScreenContext(
    authStore
);

const environmentsContext = new EnvironmentsScreenContext(environmentsStore);

const requestsScreenContext = new RequestsScreenContext(
    new RequestsStore(authStore, {
        load: loadRequests,
        save: saveRequest,
        fetchHttpRequest: fetchHttpRequest,
    })
);

function render() {
    ReactDOM.createRoot(document.getElementById("root"))
        .render(
            <React.StrictMode>
                <RequestsContext.Provider value={requestsScreenContext}>
                    <EnvironmentsContext.Provider value={environmentsContext}>
                        <AuthContext.Provider value={authScreenContext}>
                            <UIContext>
                                <App navigation={navigation} />
                            </UIContext>
                        </AuthContext.Provider>
                    </EnvironmentsContext.Provider>
                </RequestsContext.Provider>
            </React.StrictMode>
        );
}

render();