import { Menu, MenuItemConstructorOptions } from "electron";

export type ActionMap = {
    showRequests: () => Promise<void>,
    showEnvironments: () => Promise<void>,
    showAuthentication: () => Promise<void>,
    learnMore: () => Promise<void>,
}

const template = (actions: ActionMap): Array<MenuItemConstructorOptions> => [
    {
        role: 'fileMenu',
    },
    {
        role: 'editMenu'
    },
    {
        label: 'View',
        submenu: [
            {
                label: 'Requests',
                accelerator: 'F1',
                click: async () => await actions.showRequests()
            },
            {
                label: 'Environments',
                accelerator: 'F2',
                click: async () => await actions.showEnvironments()
            },
            {
                label: 'Authentication',
                accelerator: 'F3',
                click: async () => await actions.showAuthentication()
            },
            {
                type: 'separator'
            }
        ]
    },
    {
        role: 'window',
        submenu: [
            {
                role: 'minimize'
            },
            {
                role: 'close'
            }
        ]
    },
    {
        label: 'Help',
        submenu: [
            {
                label: 'Learn More',
                click: async () => await actions.learnMore()
            }
        ]
    }
];

export const menu = (actions: ActionMap): Menu => Menu.buildFromTemplate(template(actions));