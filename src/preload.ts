// See the Electron documentation for details on how to use preload scripts:
// https://www.electronjs.org/docs/latest/tutorial/process-model#preload-scripts

import { IpcRendererEvent, contextBridge, ipcRenderer } from "electron";



contextBridge.exposeInMainWorld("electronAPI", {
    onFetchAuth: (auth: any) => ipcRenderer.invoke('fetch:oauth2Creds', auth), // Renderer to main

    onLoadAuth: () => ipcRenderer.invoke('storage:loadAuth'),
    onSaveAuth: (auth: Record<string, any>) => ipcRenderer.invoke("storage:saveAuth", auth),

    onLoadRequests: () => ipcRenderer.invoke('storage:loadRequests'),
    onSaveRequest: (request: Record<string, any>) => {
        ipcRenderer.invoke('storage:saveRequest', request);
    },
    onFetchHttp: (fetchRequest: Record<string, any>) => ipcRenderer.invoke('fetch:httpRequest', fetchRequest),

    onShowRequests: (callback: (event: IpcRendererEvent) => void) => ipcRenderer.on('show-requests-view', callback), // main to renderer
    onShowEnvironments:  (callback: (event: IpcRendererEvent) => void) => ipcRenderer.on('show-environments-view', callback),
    onShowAuthentication: (callback: (event: IpcRendererEvent) => void) => ipcRenderer.on('show-authentication-view', callback)

});