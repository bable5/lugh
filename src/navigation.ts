import { makeAutoObservable } from "mobx";

export enum DESTINATIONS {
    AUTH = "auth",
    ENVIRONMENTS = "environments",
    REQUESTS = "requests"
}

export class Navigation {
    currentScreen: string | undefined = undefined;
    history: string[] = [];

    constructor() {
        makeAutoObservable(this);
    }

    gotoAuthentication = () => {
        this.navigateTo(DESTINATIONS.AUTH);
    };

    gotoEnvironments = () => {
        this.navigateTo(DESTINATIONS.ENVIRONMENTS);
    };

    gotoRequests = () => {
        this.navigateTo(DESTINATIONS.REQUESTS);
    };

    back = () => {
        if (this.history.length > 0) {
            this.currentScreen = this.history.pop();
        } else {
            this.currentScreen = undefined;
        }
    };

    private navigateTo = (screenName: DESTINATIONS) => {
        if (this.currentScreen) {
            this.history.push(this.currentScreen);
        }
        this.currentScreen = screenName;
    };
}

export const navigation = new Navigation();
