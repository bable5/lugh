import { computed, makeAutoObservable, reaction, runInAction } from "mobx";
import { KeyedStoreTransport } from ".";

export type Environment = {
    name: string,
    variables: EnvironmentVariable[]
}

export type EnvironmentVariable = {
    name: string,
    value: string,
}

class MobxEnvironment implements Environment {
    autoSave: boolean;
    saveHandler: () => void;
    dispose: () => void;
    store: EnvironmentsStore;

    name: string;
    variables: EnvironmentVariable[];

    constructor(store: EnvironmentsStore, name: string) {
        makeAutoObservable(this, {
            name: false,
            store: false,
            autoSave: false,
            saveHandler: false,
            dispose: false
        });
        this.store = store;
        this.name = name;
        this.saveHandler = reaction(
            () => ({}),
            async (json: Environment) => {
                if (this.autoSave) {
                    await this.store._transport.save(json);
                }
            }
        );
    }

    toPlainJson = (): Environment => ({
        name: this.name,
        variables: { ...this.variables }
    });

    updateFromJson(request: Record<string, unknown>) {
        this.autoSave = false;
        this.name = request.name as string;
        this.variables = request.variables as EnvironmentVariable[];
        this.autoSave = true;
    }

    newEnvironVariable() {
        this.variables.push({name: "", value: ""});
    }

    setVariable(index: number, value: string) {
        if(index >= 0 && index < this.variables.length) {
            this.variables[index].value = value;
            console.log(this.variables[index].value);
        }
    }

    setVariableName(index: number, newName: string) {
        if(index >= 0 && index < this.variables.length) {
            this.variables[index].name = newName;
        }
    }

    deleteVariable(index: number) {
        if(index >= 0 && index < this.variables.length) {
            this.variables.splice(index, 1);
        }
    }
}

export type EnvironmentStoreTransport
    = KeyedStoreTransport<Record<string, Environment>, Environment>

export class EnvironmentsStore {
    _environments: Record<string, MobxEnvironment> = {};
    _selectedEnvironment: string | undefined = undefined;
    _transport: EnvironmentStoreTransport;

    constructor(transport: EnvironmentStoreTransport) {
        makeAutoObservable(this, {
            _transport: false,
            environmentNames: computed,
            selectedEnvironment: computed
        }, {
            autoBind: true
        });
        this._transport = transport;
        this.loadEnvironments();
    }

    loadEnvironments() {
        return this._transport
            .load()
            .then(environments => runInAction(() => {
                Object.values(environments).forEach(this.updateEnvironmenFromTransport);
            }));
    }

    updateEnvironmenFromTransport(json: Environment) {
        let environment = this._environments[json.name];

        if (!environment) {
            environment = new MobxEnvironment(this, json.name);
        }

        environment.updateFromJson(json);

        this._environments[environment.name] = environment;
    }

    newEnvironmentVariable(): void {
        if (this._selectedEnvironment) {
            const environment = this._environments[this._selectedEnvironment];
            environment.newEnvironVariable();
        }
    }

    updateEnvironmentVariableKey(index: number, newName: string): void {
        if (this._selectedEnvironment) {
            const environment = this._environments[this._selectedEnvironment];
            environment.setVariableName(index, newName);
        }
    }

    updateEnvironmentVariableValue(index: number, value: string): void {
        if (this._selectedEnvironment) {
            const environment = this._environments[this._selectedEnvironment];
            environment.setVariable(index, value);
        }
    }

    deleteEnvironmentVariable(index: number): void {
        if (this._selectedEnvironment) {
            const environment = this._environments[this._selectedEnvironment];
            environment.deleteVariable(index);
        }
    }

    get environmentNames(): string[] {
        return Object.keys(this._environments).sort();
    }

    get selectedEnvironmentName(): string {
        return this._selectedEnvironment;
    }

    set selectedEnvironmentName(environmentName: string) {
        this._selectedEnvironment = environmentName;
    }

    get selectedEnvironment(): Environment {
        return this._environments[this._selectedEnvironment];
    }
}
