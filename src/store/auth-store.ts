import { action, reaction, makeAutoObservable, runInAction } from 'mobx';
import { invokeFetchAuth } from '../auth/render-credentials-fetcher';
import { v4 as uuidv4 } from 'uuid';


//TODO: Types need to be in a file main and render processes can share sanely.
export enum AuthType {
    Basic = "BASIC",
    Oauth2 = "OAUTH_2",
}

export enum Oauth2Types {
    AuthorizationToken = "AUTHORIZATION_TOKEN",
    ClientCredentials = "CLIENT_CREDENTIALS",
}

export type ClientCredentialsAuth = {
    authType: AuthType
    credentialType: Oauth2Types
    id: string
    clientKey: string,
    clientSecret: string,
    tokenUrl: string
}

export type UserCredentialsAuth = ClientCredentialsAuth & {
    authorizationUrl: string,
    callbackUrl: string,
    nonce?: string,
    scopes: string,
}

export interface Auth {
    id: string,
    name: string,
    authType: AuthType
    updateName: (name: string) => void
    toPlainJson(): Record<string, any>
    updateFromJson(auth: Record<string, any>): void;
}


export class BasicAuth implements Auth {
    authType: AuthType = AuthType.Basic;
    name = "";
    id = "";
    store: AuthStore;

    constructor(store: AuthStore, id = `${uuidv4()}`) {
        makeAutoObservable(this, {
            id: false,
            store: false,
            autoSave: false,
            saveHandler: false,
            dispose: false
        } as any); //TODO: What type is this actually? Some of these things come from being observable.
        this.store = store;
        this.id = id;
        (this as any).saveHandler = reaction(
            () => this.toPlainJson(),
            (json: Record<string, any>) => {
                if ((this as any).autoSave) {
                    this.store.authStoreTransport.save(json);
                }
            }
        );
    }

    updateFromJson(auth: Auth) {
        (this as any).autoSave = false;
        this.name = auth.name;
        (this as any).autoSave = true;
    }

    updateName(name: string) {
        this.name = name;
    }
    toPlainJson = () => ({
        id: this.id,
        name: this.name
    });
}

// TODO: Generalize with the KeyedStoreTransport.
// The interface is probably something else like `Transport`
export type AuthStoreTransport = {
    load: () => Promise<Auth[]>
    save: (auth: Record<string, any>) => Promise<void>
}

export class Oauth2Auth implements Auth {
    autoSave: boolean;
    saveHandler: () => void;
    dispose: () => void;

    authType: AuthType = AuthType.Oauth2;
    credentialType: Oauth2Types = Oauth2Types.AuthorizationToken;
    id = "";
    name = "";
    authorizationUrl = "";
    tokenUrl = "";
    clientKey = "";
    clientSecret = "";
    callbackUrl = "";
    nonce = "";
    scopes = "";
    token = "";
    store: AuthStore;

    constructor(store: AuthStore, id = `${uuidv4()}`) {
        makeAutoObservable(this, {
            id: false,
            store: false,
            autoSave: false,
            saveHandler: false,
            dispose: false
        });
        this.store = store;
        this.id = id;
        this.authType = AuthType.Oauth2;
        this.saveHandler = reaction(
            () => this.toPlainJson(),
            async (json: Record<string, any>) => {
                if (this.autoSave) {
                    await this.store.authStoreTransport.save(json);
                }
            }
        );
        this.autoSave = true;
    }

    toPlainJson = (): Record<string, any> => ({
        id: this.id,
        authType: this.authType,
        credentialType: this.credentialType,
        name: this.name,
        authorizationUrl: this.authorizationUrl,
        tokenUrl: this.tokenUrl,
        clientKey: this.clientKey,
        clientSecret: this.clientSecret,
        callbackUrl: this.callbackUrl,
        nonce: this.nonce,
        scopes: this.scopes,
        token: this.token
    });

    //TODO: What if something is missing?
    updateFromJson(oauth2: Record<string, any>) {
        this.autoSave = false;
        this.name = oauth2.name;
        this.authType = oauth2.authType;
        this.credentialType = oauth2.credentialType;
        this.authorizationUrl = oauth2.authorizationUrl;
        this.tokenUrl = oauth2.tokenUrl;
        this.clientKey = oauth2.clientKey;
        this.clientSecret = oauth2.clientSecret;
        this.callbackUrl = oauth2.callbackUrl;
        this.nonce = oauth2.nonce;
        this.scopes = oauth2.scopes;
        this.token = oauth2.token;
        this.autoSave = true;
    }

    updateName = (name: string) => {
        this.name = name;
    };

    updateAuthorizationUrl = (authUrl: string) => {
        this.authorizationUrl = authUrl;
    };

    updateTokenUrl = (tokenUrl: string) => {
        this.tokenUrl = tokenUrl;
    };

    updateCientKey = (clientKey: string) => {
        this.clientKey = clientKey;
    };

    updateClientSecret = (clientSecret: string) => {
        this.clientSecret = clientSecret;
    };

    updateCallbackUrl = (callbackUrl: string) => {
        this.callbackUrl = callbackUrl;
    };

    updateScopes = (scopes: string) => {
        this.scopes = scopes;
    };

    updateNonce = (nonce: string) => {
        this.nonce = nonce;
    };

    updateToken = (token: string) => {
        this.token = token;
    };
}

export class AuthStore {
    savedAuth: Auth[] = []; //TODO: mobx observable array gotchas?
    selectedAuth: Auth | undefined = undefined;
    authStoreTransport: AuthStoreTransport;

    constructor(authStoreTransport: AuthStoreTransport) {
        makeAutoObservable(this);
        this.authStoreTransport = authStoreTransport;
        this.loadAuth();
    }

    loadAuth = () => {
        // TODO: Loading spinner?
        this.authStoreTransport.load().then(auths => {
            runInAction(() => {
                auths.forEach(this.updateAuthfromTransport);
            });
        });
    };

    /**
     * From: https://mobx.js.org/defining-data-stores.html
     * @param json
     */
    updateAuthfromTransport = (json: Auth) => {
        let auth = this.savedAuth.find(auth => auth.id === json.id);
        if (!auth) {
            if (json.authType === AuthType.Basic) {
                auth = new BasicAuth(this, json.id);
            } else if (json.authType === AuthType.Oauth2) {
                auth = new Oauth2Auth(this, json.id);
            }
        }

        if (auth) {
            auth.updateFromJson(json);
            this.savedAuth.push(auth);
        }
    };

    createNewAuth = (authType: AuthType, options?: { oauth2Type: Oauth2Types }) => {
        if (authType === AuthType.Oauth2) {
            let oauth2Type = Oauth2Types.AuthorizationToken;
            if (options?.oauth2Type) {
                oauth2Type = options.oauth2Type;
            }

            this.selectedAuth = new Oauth2Auth(this);
            (this.selectedAuth as Oauth2Auth).credentialType = oauth2Type;


        } else if (authType == AuthType.Basic) {
            this.selectedAuth = new BasicAuth(this);
        }
        // TODO: Mobx is complaining about modifying not being in an action.
        this.addSavedAuth(this.selectedAuth);
    };

    fetchAuth = () => {
        if (this.selectedAuth) {
            const plainAuth = this.selectedAuth.toPlainJson();
            //TODO: DI this somehow?
            invokeFetchAuth(plainAuth).then(
                action("token success", (token: string) => {
                    (this.selectedAuth as Oauth2Auth).token = token; //TODO: Fix the cast.
                }),
                action("token fetch error", error => {
                    console.error("Fetching token failed", error);
                })
            );
        } else {
            console.warn("No auth selected to fetch!");
        }
    };

    addSavedAuth = (auth: Auth) => {
        this.savedAuth.push(auth);
    };

    selectAuthById = (id: string) => {
        const auth = this.savedAuth.find(a => a.id === id);
        if (auth) {
            this.selectedAuth = auth;
        }
    };
}