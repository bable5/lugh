//TODO: This isn't the correct general shape.
export type KeyedStoreTransport<L, S> = {
    load: () => Promise<L>
    save: (req: S) => Promise<void>
}
