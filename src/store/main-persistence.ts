import { Auth } from "./auth-store";
import ElectronStore from "electron-store";

const persistentAuthStore = new ElectronStore({name: "auth"});
const persistentRequestsStore = new ElectronStore({name: "requests"});


async function loadAuth(): Promise<Auth[]> {
    const allAuth = persistentAuthStore.get("auth", {}) as Record<string, any>;

    return Promise.resolve(Object.values(allAuth));
}

async function saveAuth(event: any, arg: Record<string, any>): Promise<void> {
    const allAuth = persistentAuthStore.get("auth", {}) as Record<string, any>;
    allAuth[arg.id] = arg;
    persistentAuthStore.set("auth", allAuth);

    return Promise.resolve();
}

export function registerIpcMainLoadAuthHandler(ipcMain: Electron.IpcMain, eventId: string){
    ipcMain.handle(eventId, loadAuth);
}

export function registerIpcMainSaveAuthHandler(ipcMain: Electron.IpcMain, eventId: string){
    ipcMain.handle(eventId, saveAuth);
}

async function loadRequests() {
    const requestCollectionName = "default-collection";
    const savedRequests = persistentRequestsStore.get(requestCollectionName, {}) as Record<string, any>;
    return Promise.resolve(savedRequests);

}

async function saveRequest(event: any, arg: Record<string, string>) {
    const requestCollectionName = "default-collection";
    const defaultCollection = persistentRequestsStore.get(requestCollectionName, {}) as Record<string, any>;
    defaultCollection[arg.id] = arg;
    persistentRequestsStore.set(requestCollectionName, defaultCollection);

    return Promise.resolve();
}

export function registerIpcMainLoadRequestsHandler(ipcMain: Electron.IpcMain, eventId: string){
    ipcMain.handle(eventId, loadRequests);
}

export function registerIpcMainSaveRequestHandler(ipcMain: Electron.IpcMain, eventId: string){
    ipcMain.handle(eventId, saveRequest);
}


