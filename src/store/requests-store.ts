import { action, makeAutoObservable, reaction, runInAction } from 'mobx';
import { v4 as uuidv4 } from 'uuid';
import { Auth, AuthStore } from './auth-store';

export enum RequestType {
    HTTP = "HTTP",
    GRAPH_QL = "GRAPH_QL"
}

export type Request = {
    id: string
    name: string
    url: string
    headers: Array<[string, string]>
    authId?: string
    httpVerb: string
    body: string
    graphqlQuery: string
    graphqlVariables: string
    requestType: RequestType
    lastResponse?: Record<string, any>
}

export type AuthedRequest = Request & Auth

export type RequestsStoreTransport = {
    load: () => Promise<Record<string, Request>>
    save: (req: Request) => Promise<void>
    fetchHttpRequest: (record: Record<string, any>) => Promise<any>
}

type RequestViewProps = {
    fetching: boolean
    requestFailed: boolean
}

export class HttpRequest implements Request {
    autoSave: boolean;
    saveHandler: () => void;
    dispose: () => void;
    store: RequestsStore;

    id = "";
    name = "";
    url = "";
    headers: Array<[string, string]> = [];
    _authId = "";
    _httpVerb = "";
    _body = "";
    _graphqlQuery = "";
    _graphqlVariables = "";
    _requestType = RequestType.HTTP;
    lastResponse: Record<string, any> = undefined; //TODO: Properly model this

    viewProps: RequestViewProps = {
        fetching: false,
        requestFailed: false,
    };


    constructor(store: RequestsStore, id = `${uuidv4()}`) {
        makeAutoObservable(this, {
            id: false,
            store: false,
            autoSave: false,
            saveHandler: false,
            dispose: false,
        }, { autoBind: true });
        this.store = store;
        this.id = id;
        this.saveHandler = reaction(
            () => this.toPlainJson(),
            async (json: Request) => {
                if (this.autoSave) {
                    await this.store.transport.save(json);
                }
            }
        );
        this.autoSave = true;
    }

    toPlainJson = (): Request => ({
        id: this.id,
        name: this.name,
        url: this.url,
        headers: this.headers.map(h => [...h]),
        authId: this.authId,
        httpVerb: this.httpVerb,
        requestType: this.requestType,
        body: this.body,
        graphqlQuery: this.graphqlQuery,
        graphqlVariables: this.graphqlVariables,
        // lastResponse: { ...this.lastResponse } // TODO: This isn't serializing for some reason.
    });

    //TODO: missing properties?
    updateFromJson(request: Record<string, any>) {
        this.autoSave = false;
        this.name = request.name;
        this.url = request.url;
        this.headers = request.headers;
        this.authId = request.authId;
        this.httpVerb = request.httpVerb;
        this.body = request.body;
        this.graphqlQuery = request.graphqlQuery;
        this.graphqlVariables = request.graphqlVariables;
        this.lastResponse = request.lastResponse;
        this.requestType = request.requestType;
        this.autoSave = true;
    }

    updateName = (name: string) => this.name = name;
    updateHttpVerb = (verb: string) => this.httpVerb = verb;
    updateUrl = (url: string) => this.url = url;

    get authId() {
        return this._authId;
    }

    set authId(authId: string) {
        this._authId = authId;
    }

    get httpVerb() {
        return this._httpVerb;
    }

    set httpVerb(verb: string) {
        this._httpVerb = verb;
    }

    get body() {
        return this._body;
    }

    set body(body: string) {
        this._body = body;
    }

    get graphqlQuery() {
        return this._graphqlQuery;
    }

    set graphqlQuery(query: string) {
        this._graphqlQuery = query;
    }

    get graphqlVariables() {
        return this._graphqlVariables;
    }

    set graphqlVariables(variables: string) {
        this._graphqlVariables = variables;
    }

    get requestType() {
        return this._requestType;
    }

    set requestType(requestType: RequestType) {
        this._requestType = requestType;
    }

    addHeader() {
        this.headers.push(["", ""]);
    }

    removeHeader(idx: number) {
        if (idx >= 0) {
            this.headers.splice(idx, 1);
        }
    }

    updateHeaderName(id: number, name: string) {
        if (id >= 0 && id < this.headers.length) {
            this.headers[id][0] = name;
        }
    }

    updateHeaderValue(id: number, value: string) {
        if (id >= 0 && id < this.headers.length) {
            this.headers[id][1] = value;
        }
    }

    fetch() {
        const plainRequest = this.toPlainJson();
        let auth: Auth;
        if (this.authId) {
            auth = this.store.authStore.savedAuth.find(a => a.id === this.authId);
        }

        const fullRequest = {
            request: plainRequest,
            auth: auth ? auth.toPlainJson() : {}
        };

        this.viewProps.fetching = true;
        this.viewProps.requestFailed = false;

        this.store.transport.fetchHttpRequest(fullRequest)
            .then(
                action("fetch success", (response: Record<string, unknown>) => {
                    this.lastResponse = response;
                    this.viewProps.fetching = false;
                    this.viewProps.requestFailed = false;
                }

                ),
                action("fetch failed", (response: Record<string, unknown>) => {
                    console.log(JSON.stringify(response, null, 2));
                    this.lastResponse = response;
                    this.viewProps.fetching = false;
                    this.viewProps.requestFailed = true;
                })
            );
    }
}

export class RequestsStore {
    requests: Record<string, HttpRequest>;
    selectedRequestId: string | undefined;
    transport: RequestsStoreTransport;
    authStore: AuthStore;

    constructor(authStore: AuthStore, transport: RequestsStoreTransport) {
        makeAutoObservable(this, {
            authStore: false,
            transport: false
        }, {
            autoBind: true
        });
        this.requests = {};
        this.transport = transport;
        this.authStore = authStore;
        this.loadRequests();
    }

    loadRequests() {
        this.transport.load().then(requests => {
            runInAction(() => {
                Object.values(requests).forEach(this.updateRequestsFromTransport);
            });
        });
    }

    createRequest() {
        const r = new HttpRequest(this);
        r.name = "NEW";
        this.requests[r.id] = r;
        this.selectRequestById(r.id);
    }

    selectRequestById(id: string) {
        this.selectedRequestId = id;
    }

    private updateRequestsFromTransport(json: Request) {
        let request = this.requests[json.id];

        if (!request) {
            request = new HttpRequest(this, json.id);
        }

        request.updateFromJson(json);

        this.requests[request.id] = request;
    }
}
