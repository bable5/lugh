import { test, expect } from "vitest";
import { Environment, EnvironmentStore, EnvironmentStoreTransport } from "./environments-store";

//TODO: How do we test this. There aren't any async's to await?
test.skip("Create a new environment store", () => {
    const expectedEnvironments = {
        "env1": {name: "env1", variables: {}},
        "env2": {name: "env2", variables: {}},
    };
    const environmentStore = givenEnvironmentsStore(expectedEnvironments);

    expect(environmentStore.environmentNames)
        .toContain(["env1", "env2"]);
});

const givenEnvironmentsStore = (expectedEnvironments: Record<string, Environment>) => {
    const transport: EnvironmentStoreTransport = {
        load: () => Promise.resolve(expectedEnvironments),
        save: vi.fn()
    };

    return new EnvironmentStore(transport);
};
