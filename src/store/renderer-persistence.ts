import { Auth } from "./auth-store";
import { Request } from './requests-store';

export function saveAuth(record: Record<string, any>): Promise<any> {
    return window
        .electronAPI
        .onSaveAuth(record);
}

export function loadAuth(): Promise<Auth[]> {
    return window
        .electronAPI
        .onLoadAuth();
}

export function saveRequest(record: Record<string, any>): Promise<void> {
    return window
        .electronAPI
        .onSaveRequest(record);
}

export function loadRequests(): Promise<Record<string, Request>> {
    return window
        .electronAPI
        .onLoadRequests();
}