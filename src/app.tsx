import { Navigation , DESTINATIONS } from "./navigation";
import { observer } from "mobx-react-lite";
import AuthScreen from "./auth/AuthScreen";
import RequestsScreen from "./requests/RequestsScreen";
import { Layout } from "antd";
import Header from "./components/Header";
import EnvironmentsScreen from "./environment/EnvironmentsScreen";

const MainView = observer(({ navigation }: { navigation: Navigation }) => {
    switch (navigation.currentScreen) {
        case DESTINATIONS.AUTH:
            return <AuthScreen />;
        case DESTINATIONS.ENVIRONMENTS:
            return <EnvironmentsScreen />;
        case DESTINATIONS.REQUESTS:
            return <RequestsScreen />;
        default:
            return <RequestsScreen />;
    }
});

export default observer(({ navigation }: { navigation: Navigation }) => {
    return <Layout>
        <Header />
        <Layout.Content>
            <MainView navigation={navigation} />
        </Layout.Content>
    </Layout>;
});

