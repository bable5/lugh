
import { makeAutoObservable } from "mobx";

class Timer {
    secondsPassed = 0;
    private tickHandle: NodeJS.Timeout;
    private isStarted = false;

    constructor() {
        makeAutoObservable(this);
    }

    increaseTimer() {
        this.secondsPassed += 1;
    }

    start() {
        if (this.isStarted) {
            throw new Error("Timer already running!");
        }
        this.isStarted = true;
        this.tickHandle = setInterval(() => {
            this.increaseTimer();
        }, 1000);
    }

    stop() {
        if(this.tickHandle) {
            clearInterval(this.tickHandle);
            this.isStarted = false;
        }
    }
}

export default Timer;