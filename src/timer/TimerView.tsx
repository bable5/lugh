import { useEffect, useState } from "react";
import Timer from './Timer';
import { observer } from "mobx-react-lite";


const TimerView = observer(() => {
    const [timer] = useState(() => new Timer());

    useEffect(() => {
        timer.start();
        return () => timer.stop();
    }, [timer]);

    return <span>Seconds passed: {timer.secondsPassed}</span>;
});

export default TimerView;