import { computed, makeAutoObservable } from 'mobx';
import { RequestsStore } from '../store/requests-store';

export enum ActiveTab {
    SHOW_BODY = "SHOW_BODY",
    SHOW_HEADERS = "SHOW_HEADERS",
    SHOW_AUTH = "SHOW_AUTH",
}

export class RequestsScreenStore {
    _activeTab: string;
    _activeResponseTab: string;
    _visibleRequestsFilter: string;
    _requestsStore: RequestsStore;

    constructor(requestsStore: RequestsStore) {
        makeAutoObservable(this, {
            visibleRequests: computed
        }, { autoBind: true });
        this._activeTab = ActiveTab.SHOW_BODY;
        this._visibleRequestsFilter = "";
        this._requestsStore = requestsStore;
    }

    get activeTab() {
        return this._activeTab;
    }

    set activeTab(activeTab: string) {
        this._activeTab = activeTab;
    }

    get activeResponseTab() {
        return this._activeResponseTab;
    }

    set activeResponseTab(activeResponseTab: string) {
        this._activeResponseTab = activeResponseTab;
    }

    get visibleRequestsFilter() {
        return this._visibleRequestsFilter;
    }

    set visibleRequestsFilter(visibleRequestsFilter: string) {
        this._visibleRequestsFilter = visibleRequestsFilter;
    }

    get visibleRequests() {
        return Object.values(this._requestsStore.requests)
            .filter(r => this._visibleRequestsFilter
                ? r.name.includes(this._visibleRequestsFilter)
                : r);
    }
}