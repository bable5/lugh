import { expect, test } from 'vitest';
import { Auth, AuthStore } from '../store/auth-store';
import { Request, RequestsStore, RequestsStoreTransport } from '../store/requests-store';
import { RequestsScreenContext } from './requests-context';

test("Adding a new request is visible should be visible", () => {
    const store = givenARequestsContext();

    store.requestsStore.createRequest();

    expect(store.requestScreenStore.visibleRequests)
        .to.toHaveLength(1);
});

test("Adding a filter should hide reqeusts that do not match", () => {
    const store = givenARequestsContext();

    store.requestsStore.createRequest();

    const request = Object.values(store.requestsStore.requests)[0];
    request.name = "FOO";
    store.requestScreenStore.visibleRequestsFilter = "B";

    expect(store.requestScreenStore.visibleRequests)
        .to.toHaveLength(0);
});

const givenARequestsContext = (): RequestsScreenContext => {

    const mockAuthStore = new AuthStore({
        load: () => Promise.resolve([] as Auth[]),
        save: vi.fn()
    });
    const requestStoreTransport: RequestsStoreTransport = {
        load: () => Promise.resolve([] as unknown as Record<string, Request>),
        save: vi.fn(),
        fetchHttpRequest: vi.fn()
    };
    const requestsStore = new RequestsStore(
        mockAuthStore,
        requestStoreTransport
    );
    return new RequestsScreenContext(requestsStore);
};