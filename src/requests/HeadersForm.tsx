import { Flex } from 'antd';
import { observer } from 'mobx-react-lite';
import { useCallback } from 'react';
import ControlledInput from '../components/forms/ControlledInput';
import { HttpRequest } from '../store/requests-store';


type HeaderRowProps = {
    name: string,
    value: string,
    idx: number,
    request: HttpRequest,
}

const HeaderRow = observer(({ name, value, idx, request }: HeaderRowProps) => {
    const onNameChange = useCallback((e: string) => {
        return request.updateHeaderName(idx, e);
    },
        [idx, request]
    );
    const onValueChange = useCallback((e: string) => request.updateHeaderValue(idx, e),
        [idx, request]
    );
    const removeHeader = useCallback(() => request.removeHeader(idx), [idx, request]);
    return (
        <Flex >
            <ControlledInput name={name} value={name} onChange={onNameChange} />
            <ControlledInput name={value} value={value} onChange={onValueChange} />
            <button onClick={removeHeader}>-</button>
        </Flex>
    );
});

const HeaderForm = observer(({ request }: { request: HttpRequest }) => {
    const addHeader = useCallback(() => {
        request.addHeader();
    }, [request]);

    return (<>
        <button onClick={addHeader}>+</button>
        <Flex vertical={true}>
            {request.headers.map(([k, v], idx) => <HeaderRow key={idx} name={k} value={v} idx={idx} request={request} />)}
        </Flex>
    </>
    );
});

export default HeaderForm;