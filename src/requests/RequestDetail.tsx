import { Button, Flex, Select, Space, Spin, Typography } from 'antd';
import { observer } from "mobx-react-lite";
import { useCallback, useContext } from "react";
import { AuthContext } from "../auth/auth-context";
import ControlledInput from "../components/forms/ControlledInput";
import { HttpRequest } from '../store/requests-store';
import HeadersForm from "./HeadersForm";
import HttpVerbSelect from "./HttpVerbSelect";
import RequestBody from './RequestBody';
import RequestFormTabSelect from './RequestFormTabSelect';
import RequestTypeSelect from './RequestTypeSelect';
import ResponseDetails from './ResponseDetails';
import { ActiveTab } from './request-screen-store';
import { RequestsContext } from "./requests-context";

const { Title } = Typography;

type RequestFormProps = {
    request: HttpRequest
}

const SelectAuth = observer(({ request }: RequestFormProps) => {
    const { authStore } = useContext(AuthContext);
    const selectedAuthId = request.authId;

    const handleSelect = useCallback((selectedAuthId: string) => {
        request.authId = selectedAuthId;
    }, [request]);

    const authOptions = authStore.savedAuth.map(auth => ({
        label: auth.name,
        value: auth.id,
    }));

    return (
        <Select value={selectedAuthId} onChange={handleSelect}
            showSearch
            placeholder={"Select Authorization"}
            optionFilterProp="label"
            options={authOptions}
        />
    );
});

const ActiveTabContents = observer(({ request }: RequestFormProps) => {
    const requestScreenStore = useContext(RequestsContext).requestScreenStore;

    switch (requestScreenStore.activeTab) {
        case ActiveTab.SHOW_BODY:
            return <RequestBody request={request} />;
        case ActiveTab.SHOW_AUTH:
            return <SelectAuth request={request} />;
        case ActiveTab.SHOW_HEADERS:
            return <HeadersForm request={request} />;
        default:
            return undefined;
    }
});

const RequestFormTab = observer(({ request }: RequestFormProps) => {

    return (<>
        <RequestTypeSelect request={request} />
        <HttpVerbSelect request={request} />
        <ControlledInput name="url" value={request.url} onChange={request.updateUrl} />
        {
            request.viewProps.fetching ?
                <Space><Spin /></Space> :
                <Button onClick={request.fetch}>send</Button>
        }
    </>);
});



const RequestForm = observer(({ request }: RequestFormProps) => {
    return (
        <Flex>
            <Flex vertical flex="1">
                <ControlledInput name="name" value={request.name} onChange={request.updateName} />
                <Flex><RequestFormTab request={request} /></Flex>
                <RequestFormTabSelect />
                <ActiveTabContents request={request} />
            </Flex>
            <Flex vertical flex="1">
                <ResponseDetails request={request} />
            </Flex>
        </Flex>
    );
});

const RequestDetail = observer(() => {
    const requestStore = useContext(RequestsContext).requestsStore;
    const selectedRequest = requestStore.requests[requestStore.selectedRequestId];

    return requestStore.selectedRequestId ?
        <RequestForm request={selectedRequest} />
        : <Flex justify='center'><Title level={2}>Select a request</Title></Flex>;
});

export default function () {
    return <Flex vertical >
        <RequestDetail />
    </Flex>;
}