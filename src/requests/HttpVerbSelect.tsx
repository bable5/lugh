import { observer } from "mobx-react-lite";
import { Request } from '../store/requests-store';
import { useCallback } from "react";
import { Select } from 'antd';


const verbs = [
    { value: "GET", label: "GET" },
    { value: "POST", label: "POST" },
    { value: "PUT", label: "PUT" },
    { value: "PATCH", label: "PATCH" },
    { value: "DELETE", label: "DELETE" },
    { value: "OPTIONS", label: "OPTIONS" }
];

const HttpVerbSelect = observer(({ request }: { request: Request }) => {

    const selectVerb = useCallback((value: any) => request.httpVerb = value, [request]);

    return (
        <Select value={request.httpVerb}
            onChange={selectVerb}
            defaultValue="GET"
            options={verbs}
            placeholder="Env"
            style={{width: 200}}
        />
    );
});

export default HttpVerbSelect;
