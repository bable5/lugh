import ListDetail from "../components/ListDetail";
import RequestDetail from "./RequestDetail";
import RequestList from "./RequestList";

export default function () {


    return (
        <ListDetail
            list={RequestList}
            detail={RequestDetail}
        />
    );
}