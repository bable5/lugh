export function fetchHttpRequest(request: Record<string, any>) {
    return (window as any)
        .electronAPI
        .onFetchHttp(request);
}
