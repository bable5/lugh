import { observer } from "mobx-react-lite";
import { Request , RequestType } from '../store/requests-store';
import { useCallback } from "react";

import { Select } from 'antd';


const requestTypes = [
    { value: RequestType.HTTP, label: "Http" },
    { value: RequestType.GRAPH_QL, label: "GraphQL" },
];

const RequestTypeSelect = observer(({ request }: { request: Request }) => {
    const selectRequestType = useCallback((value: any) =>
        request.requestType = value, [request]);

    return (
        <Select value={request.requestType}
            onChange={selectRequestType}
            defaultValue={RequestType.HTTP}
            options={requestTypes}
            placeholder={"Request Type"}
            style={{width: 100}}
        />
    );
});

export default RequestTypeSelect;