
export function registerIpcMainFetchHttpRequest(ipcMain: Electron.IpcMain, eventId: string) {
    ipcMain.handle(eventId, handleFetchHttpRequest);
}

async function handleFetchHttpRequest(event: any, arg: Record<string, any>) {
    const headers: Record<string, string> =
        arg
            .request
            .headers
            .reduce((acc: Record<string, string>, [key, value]: [string, string]) => {
                if (key) {
                    acc[key] = value;
                }
                return acc;
            }, {});

    addAuthorization(headers, arg['auth']);

    //TODO: Error handling
    try {

        const req: RequestInit = {
            method: arg.request.httpVerb,
            headers,
        };

        req['body'] = buildRequestBody(arg.request);

        const response = await fetch(arg.request.url, req);
        const body = await response.text();
        return {
            status: response.status,
            headers: Array.from(response.headers),
            body: body
        };
    } catch (err) {
        return {
            err: err.message
        };
    }
}

function addAuthorization(headers: Record<string, string>, auth: Record<string, string>) {
    if (auth && auth['authType'] === 'OAUTH_2' && auth['token']) {
        headers['Authorization'] = `Bearer ${auth['token']}`;
    }
}

function buildRequestBody(request: any) {
    if (request.body && request.verb != 'GET' && request.vert != 'HEAD') {
        return request.body;
    } else if (request.graphqlQuery) { // TODO: Check that method is POST?
        const jsonQuery: Record<string, any> = {
            "query": `${request.graphqlQuery}`
        };
        if (request.graphqlVariables) {
            jsonQuery["variables"] = JSON.parse(request.graphqlVariables);
        }
        return JSON.stringify(jsonQuery);
    }
}
