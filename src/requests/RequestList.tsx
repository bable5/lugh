import { Layout, List } from "antd";
import { observer } from "mobx-react-lite";
import { useContext } from "react";
import ControlledInput from "../components/forms/ControlledInput";
import ScrollView from "../components/ScrollView";
import { HttpRequest } from "../store/requests-store";
import { RequestsContext } from "./requests-context";

const RequestListFilter = observer(() => {
    const { requestScreenStore } = useContext(RequestsContext);
    const doFilter = (v: string) => requestScreenStore.visibleRequestsFilter = v;

    return (<ControlledInput
        name={"requsts-filter"}
        value={requestScreenStore.visibleRequestsFilter}
        onChange={doFilter}
    />
    );
});

const RequestListHeader = () => {
    const requestStore = useContext(RequestsContext).requestsStore;
    return <button onClick={requestStore.createRequest}>+</button>;
};

const ListItemObserver = observer(({ item }: { item: HttpRequest }) =>
    <div>{item.name}</div>
);

const RequestListView = observer(() => {
    const { requestsStore, requestScreenStore } = useContext(RequestsContext);
    const visibleRequests = requestScreenStore.visibleRequests;

    return (<>
        <RequestListFilter />
        <ScrollView>
            <List
                dataSource={visibleRequests}
                renderItem={item => (
                    <List.Item
                        key={item.id}
                        onClick={() => { requestsStore.selectRequestById(item.id); }}
                    >
                        <ListItemObserver item={item} />
                    </List.Item>
                )}

            />
        </ScrollView>
    </>
    );
});

export default () => (
    <Layout>
        <RequestListHeader />
        <RequestListView />
    </Layout>
);
