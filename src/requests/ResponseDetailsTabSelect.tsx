import { observer } from "mobx-react-lite";
import { useCallback, useContext } from "react";
import { RequestsContext } from "./requests-context";
import { Menu, MenuProps } from "antd";

export enum ActiveTab {
    SHOW_BODY = "SHOW_BODY",
    SHOW_HEADERS = "SHOW_HEADERS"
}

const ResponseDetailTabs: MenuProps['items'] = [
    {
        label: 'Headers',
        key: 'show_headers'
    }, {
        label: 'Body',
        key: 'show_body'
    }

];

export default observer(() => {
    const requestScreenStore = useContext(RequestsContext).requestScreenStore;

    //TODO: Different store/state management for this. It's easy to effect the wrong pannel
    const showBody = useCallback(() => requestScreenStore.activeResponseTab = ActiveTab.SHOW_BODY, []);
    const showHeaders = useCallback(() => requestScreenStore.activeResponseTab = ActiveTab.SHOW_HEADERS, []);

    const onClick: MenuProps['onClick'] = useCallback((e) => {
        switch(e.key) {
            case 'show_headers':
                return showHeaders();
            case 'show_body':
                return showBody();
            default:
                // pass
        }
      }, []);

    return (
        <Menu onClick={onClick} mode='horizontal' items={ResponseDetailTabs} />
    );
});