import { Input, List, Typography } from "antd";
import { observer } from "mobx-react-lite";
import { useContext } from "react";
import { HttpRequest } from '../store/requests-store';
import { ActiveTab } from "./request-screen-store";
import ResponseDetailsTabSelect from "./ResponseDetailsTabSelect";
import { RequestsContext } from "./requests-context";
import JsonLiteral from "../components/JsonLiteral";
import ScrollView from "../components/ScrollView";

const { TextArea } = Input;
const { Text } = Typography;


const ResponseHeaders = observer(({ response }: { response: Record<string, any> }) => {
    if (response.headers) {
        return (
            <List bordered
                size="small"
                dataSource={response['headers']}
                renderItem={(item: [string, string]) => (
                    <List.Item>
                        <Text>{item[0]}:</Text> <Text>{item[1]}</Text>
                    </List.Item>
                )}
            >
            </List>
        );
    }

    return undefined;
});

const ResponseBody = observer(({ response }: { response: Record<string, any> }) => {
    const body = response['body']; //TODO: Understand content type when dealing with the body.
    const err = response['err'];
    try {
        if (body) {
            return <JsonLiteral obj={JSON.parse(body)} />;
        } else if (err) {
            return <p>{err}</p>;
        } else {
            return <p>The response had neither a body nor an error</p>;
        }
    } catch (error) {
        console.log(error);
        return (
            <TextArea
                readOnly
                autoSize
                value={body}
                style={{
                    fontFamily: 'monospace'
                }}
            />);
    }
});

const ResponseDetailsTab = observer(({ response }: { response: Record<string, any> }) => {
    const requestScreenStore = useContext(RequestsContext).requestScreenStore;

    switch (requestScreenStore.activeResponseTab) {
        case ActiveTab.SHOW_HEADERS:
            return <ResponseHeaders response={response} />;
        default: return <ResponseBody response={response} />;
    }
});

const ResponseDetails = observer(({ request }: { request: HttpRequest }) => {
    if (!request.lastResponse) {
        return (<div></div>);
    }

    return (
        <>
            <ResponseDetailsTabSelect />
            <Text >Status: {request.lastResponse["status"]}</Text>
            <ScrollView>
                <ResponseDetailsTab response={request.lastResponse} />
            </ ScrollView>
        </>
    );
});

export default ResponseDetails;