import { Menu, MenuProps } from 'antd';
import { observer } from "mobx-react-lite";
import { useCallback, useContext } from "react";
import { RequestsContext } from "./requests-context";
import { ActiveTab } from './request-screen-store';

const ReqeustDetailTabs: MenuProps['items'] = [
    {
        label: 'Body',
        key: 'show_body'
    },
    {
        label: 'Headers',
        key: 'show_headers'
    }, {
        label: 'Auth',
        key: 'show_auth'
    }

];

export default observer(() => {
    const requestScreenStore = useContext(RequestsContext).requestScreenStore;

    const showBody = useCallback(() => requestScreenStore.activeTab = ActiveTab.SHOW_BODY, []);
    const showHeaders = useCallback(() => requestScreenStore.activeTab = ActiveTab.SHOW_HEADERS, []);
    const showAuth = useCallback(() => requestScreenStore.activeTab = ActiveTab.SHOW_AUTH, []);

    const onClick: MenuProps['onClick'] = useCallback((e) => {
        switch (e.key) {
            case 'show_body':
                return showBody();
            case 'show_headers':
                return showHeaders();
            case 'show_auth':
                return showAuth();
            default:
            // pass
        }
    }, []);

    return (
        <Menu onClick={onClick} mode='horizontal' items={ReqeustDetailTabs} />
    );
});