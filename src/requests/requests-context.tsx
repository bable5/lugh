import { createContext } from 'react';
import { RequestsStore } from '../store/requests-store';
import { RequestsScreenStore } from './request-screen-store';

export class RequestsScreenContext {
    private _requestsStore: RequestsStore;
    private _requestsScreenStore: RequestsScreenStore;

    constructor(requestsStore: RequestsStore) {
        this._requestsStore = requestsStore;
        this._requestsScreenStore = new RequestsScreenStore(requestsStore);
    }

    get requestsStore(): RequestsStore {
        return this._requestsStore;
    }

    get requestScreenStore(): RequestsScreenStore {
        return this._requestsScreenStore;
    }
}

export const RequestsContext: React.Context<RequestsScreenContext> = createContext(undefined);
