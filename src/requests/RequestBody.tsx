import { Flex, Input, Layout, theme, Typography } from "antd";
import { observer } from "mobx-react-lite";
import { useCallback } from "react";
import { HttpRequest, RequestType } from "../store/requests-store";

const { Title } = Typography;

const { TextArea } = Input;
const { Content } = Layout;

type RequestBodyProps = {
    request: HttpRequest
}

const RequestBodyDetail = observer((props: RequestBodyProps) => {
    switch (props.request.requestType) {
        case RequestType.HTTP:
            return <SimpleBody {...props} />;
        case RequestType.GRAPH_QL:
            return <GraphQLBody {...props} />;
        default:
            return <SimpleBody {...props} />;
    }
});

const SimpleBody = observer(({ request }: RequestBodyProps) => {
    const onRequestBodyChange = useCallback((e: React.ChangeEvent<HTMLTextAreaElement>) => {
        e.preventDefault();
        request.body = e.target.value;
    }, [request]);

    return (
        <TextArea autoFocus
            onChange={onRequestBodyChange}
            value={request.body}
            style={{
                fontFamily: 'monospace'
            }}
            autoSize
        />
    );
});

const GraphQLBody = observer(({ request }: RequestBodyProps) => {
    const onRequestQueryChange = useCallback((e: React.ChangeEvent<HTMLTextAreaElement>) => {
        e.preventDefault();
        request.graphqlQuery = e.target.value;
    }, [request]);

    const onRequestVariablesChange = useCallback((e: React.ChangeEvent<HTMLTextAreaElement>) => {
        e.preventDefault();
        request.graphqlVariables = e.target.value;
    }, [request]);

    return (
        <Flex vertical>
            <Title level={5}>Query</Title>
            <TextArea autoFocus
                onChange={onRequestQueryChange}
                value={request.graphqlQuery}
                style={{
                    fontFamily: 'monospace'
                }}
                size="large"
            />
            <Title level={5}>Variables (as Json)</Title>
            <TextArea autoFocus
                onChange={onRequestVariablesChange}
                value={request.graphqlVariables}
                style={{
                    fontFamily: 'monospace'
                }}
                size="large"
            />
        </Flex>

    );

});

export default (props: RequestBodyProps) => {

    const {
        token: { colorBgContainer, borderRadiusLG },
    } = theme.useToken();
    return (
        <Layout >
            <Content>
                <Flex vertical
                    style={{
                        background: colorBgContainer,
                        padding: '1rem',
                        borderRadius: borderRadiusLG,
                    }}
                >
                    <RequestBodyDetail {...props} />
                </Flex>
            </Content>
        </Layout>
    );
};