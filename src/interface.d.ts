import { Auth } from "./store/auth-store";
import { Request } from './store/requests-store';

export interface IElectronAPI {
    onSaveAuth: (record: Record<string, any>) => any
    onLoadAuth: () => Promise<Auth[]>
    onSaveRequest: (record: Record<string, any>) => Promise<void>
    onLoadRequests: () => Promise<Record<string, Request>>
  }

  declare global {
    interface Window {
      electronAPI: IElectronAPI
    }
  }