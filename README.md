# Lugh

## Description

[Lugh](https://en.wikipedia.org/wiki/Lugh) Yet another RESTful electron client app. Lugh aims to let users keep their data on their machines.


## License
 
[MIT](./LICENSE)

## Project status

It's the early and optimistic days.
