### 2023-12-27

* [x] Spinner for requests
* [ ] Request error handling?
* [x] Request Body
* [x] Figure out wrap on response body
* [ ] Delete request button
* [ ] Delete AUTH button
* [x] Client Credentials toggle for auth
* [ ] Unify client credentials and authorization token form

### Environment variables

* Environements should nest.
  - At least a 'base' environment and a 'current' environment
  - Environments should store secrets in the system 'native' secrets manager

### Auth

* Auth should be independant of requests.
  - A given auth should be easily attached to a given request


### Requests

* SEND button
* Headers
* Attach Auth
